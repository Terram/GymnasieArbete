Game.Preloader = function(game){

	this.preloadBar = null;
};

Game.Preloader.prototype = {
	preload:function(){
		//visar laddningsskärm
		this.preloadBar = this.add.sprite(this.world.centerX, this.world.centerY, 'preloaderBar');
		//ger position till client center karaktär
		this.preloadBar.anchor.setTo(0.5, 0.5);
		//fps min max förändras
		this.time.advancedTiming = true;
		//laddar preloadbar function
		this.load.setPreloadSprite(this.preloadBar);

		//laddar alla grejer jag behöver till spelet, map, bilder(tiles) till mapen, spelare, monster
		this.load.tilemap('map0', 'assets/level0.csv');
		this.load.tilemap('map','assets/level1.csv');
		this.load.image('tiles', 'assets/tileset.png');
		this.load.spritesheet('player','assets/sprite.png', 24, 26);
		this.load.image('clown','assets/clown.png');
		this.load.tilemap('map2','assets/level2.csv');
		this.load.tilemap('map3','assets/level3.csv');
		this.load.tilemap('map4','assets/win.csv');
	},

	create:function(){
		//startar nästa state
		this.state.start('Level0');
	}

}