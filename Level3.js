//skapar function
enemyClown4 = function(index, game, x, y){
	//lägger till clownen
	this.clown = game.add.sprite(x, y, 'clown');
	//låser den
	this.clown.anchor.setTo(0.5, 0.5);
	this.clown.name = index.toString();
	//gör att den påverkas av physics i phaser
	game.physics.enable(this.clown, Phaser.Physics.ARCADE);
	//kan inte röra
	this.clown.body.immovable = true;
	//colliderar med yttre världen
	this.clown.body.collideWorldBounds = true;
	//påverkas inte av gravitation
	this.clown.body.allowGravity = false;
	//tween för rörelse fram och tillbaka
	this.clownTween = game.add.tween(this.clown).to(
		{ x: this.clown.x + 320 }, 2500, 'Linear', true, 0, 1250, true);
}

enemyClown5 = function(index, game, x, y){
	//lägger till clownen
	this.clown = game.add.sprite(x, y, 'clown');
	this.clown.scale.setTo(1.3, 1.3);
	//låser den
	this.clown.anchor.setTo(0.5, 0.5);
	this.clown.name = index.toString();
	//gör att den påverkas av physics i phaser
	game.physics.enable(this.clown, Phaser.Physics.ARCADE);
	//kan inte röra
	this.clown.body.immovable = true;
	//colliderar med yttre världen
	this.clown.body.collideWorldBounds = true;
	//påverkas inte av gravitation
	this.clown.body.allowGravity = false;
	//tween för upp och ner
	this.clownTween = game.add.tween(this.clown).to(
		{ y: this.clown.y + 100 }, 410, 'Linear', true, 0, 2500, true);
}

enemyClown6 = function(index, game, x, y){
	//lägger till clownen
	this.clown = game.add.sprite(x, y, 'clown');
	this.clown.scale.setTo(1.3, 1.3);
	this.clown.anchor.setTo(0.5, 0.5);
	this.clown.name = index.toString();
	//gör att den påverkas av physics i phaser
	game.physics.enable(this.clown, Phaser.Physics.ARCADE);
	//kan inte röra sig från andras impakt
	this.clown.body.immovable = true;
	//colliderar med yttre världen
	this.clown.body.collideWorldBounds = true;
	//påverkas inte av gravitation
	this.clown.body.allowGravity = false;
	//tween för upp och ner
	this.clownTween = game.add.tween(this.clown).to(
		{ y: this.clown.y + 100 }, 300, 'Linear', true, 0, 2500, true);
}
//skapar variabler för saker som ska användas i mer än en function
var enemyD1;
var enemyD2;
var enemyD3;
var enemyD4;
var enemyD5;

//kommenterar inte kod som förklarats andra sidan
Game.Level3 = function (game) {};

var map;
var layer;
var player;
var controls ={};
var speed = 200;
var JumpT = 0;

Game.Level3.prototype ={
	create:function(game) {
		this.physics.arcade.gravity.y = 1000;

		this.stage.backgroundColor = '#3A5963';
		map = this.add.tilemap('map3', 32, 32);
		var tileset = map.addTilesetImage('tiles');
		
		layer = map.createLayer(0);
		layer.resizeWorld();

		map.setCollisionBetween(0, 3);

		map.setTileIndexCallback(6, this.resetPlayer, this);
		map.setTileIndexCallback(12, this.nextLevel, this);

		player = this.add.sprite(100, 460, 'player');
		player.anchor.setTo(0.5, 0.5);

		player.animations.add('idle', [0,1], 1, true);
		player.animations.add('jump', [2], 1, true);
		player.animations.add('run', [3,4,5,6,7,8], 7, true);

		this.physics.arcade.enable(player);

		this.camera.follow(player);

		player.body.collideWorldBounds = true;

		controls = {
			right: this.input.keyboard.addKey(Phaser.Keyboard.D),
			left: this.input.keyboard.addKey(Phaser.Keyboard.A),
			up: this.input.keyboard.addKey(Phaser.Keyboard.W)
		};
		enemyD1 = new enemyClown4 (0, game, player.x + 125, player.y - 95);
		enemyD2 = new enemyClown5 (0, game, player.x + 600, player.y - 260);
		enemyD3 = new enemyClown5 (0, game, player.x + 850, player.y - 260);
		enemyD4 = new enemyClown5 (0, game, player.x + 650, player.y - 260);
		enemyD5 = new enemyClown6 (0, game, player.x + 725, player.y - 260);

	},

	update: function () {
		player.body.velocity.x = 0;

		this.physics.arcade.collide(player, layer);
		if(controls.up.isDown && (player.body.onFloor() || 
			player.body.touching.down) && this.time.now > JumpT){
			player.animations.play('jump');
			player.body.velocity.y = -500;
			JumpT = this.time.now + 750;
		}
		if(controls.right.isDown){
			player.animations.play('run');
			player.scale.setTo(1, 1);
			player.body.velocity.x += speed;
		}
		if(controls.left.isDown){
			player.animations.play('run');
			player.scale.setTo(-1, 1);
			player.body.velocity.x -= speed;
		}
		if(player.body.velocity.x == 0 && player.body.velocity.y == 0){
			player.animations.play('idle');
		}

		if(checkOverlap(player, enemyD1.clown)){
			this.resetPlayer();

		}
		if(checkOverlap(player, enemyD2.clown)){
			this.resetPlayer();

		}
		if(checkOverlap(player, enemyD3.clown)){
			this.resetPlayer();

		}
		if(checkOverlap(player, enemyD4.clown)){
			this.resetPlayer();

		}
		if(checkOverlap(player, enemyD5.clown)){
			this.resetPlayer();

		}

	},	

	resetPlayer: function() {
		// (start position)
		player.reset(100, 460);
	},

	nextLevel: function() {

		this.game.state.start('win');
	},

}

	function checkOverlap(spriteA, spriteB){
		//kollar var spritesen är, lägger i var
		var boundsA = spriteA.getBounds();
		var boundsB = spriteB.getBounds();
		//kollar om de är på varandra
		return Phaser.Rectangle.intersects(boundsA, boundsB);
	}