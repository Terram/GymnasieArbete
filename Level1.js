//skapar function
enemyClown = function(index, game, x, y){
	//lägger till clownen
	this.clown = game.add.sprite(x, y, 'clown');
	//låser den
	this.clown.anchor.setTo(0.5, 0.5);
	this.clown.name = index.toString();
	//gör att den påverkas av physics i phaser
	game.physics.enable(this.clown, Phaser.Physics.ARCADE);
	//kan inte röra
	this.clown.body.immovable = true;
	//colliderar med yttre världen
	this.clown.body.collideWorldBounds = true;
	//påverkas inte av gravitation
	this.clown.body.allowGravity = false;
	//tween för rörelse fram och tillbaka
	this.clownTween = game.add.tween(this.clown).to(
		{ x: this.clown.x + 100 }, 2000, 'Linear', true, 0, 2500, true);
}
//skapar variabler för saker som ska användas i mer än en function
var enemy1;
var enemy2;
//skapar game.level1 functionen
Game.Level1 = function (game) {};

//skapar variabler för saker som ska användas i mer än en function
var map;
var layer;
var player;
var controls ={};
var speed = 200;
var JumpT = 0;
var tree = 0;
var scoreText;

Game.Level1.prototype ={
	create:function(game) {
		//gravity
		this.physics.arcade.gravity.y = 1000;
		//backgroundsfärg
		this.stage.backgroundColor = '#3A5963';
		//skapar en map
		map = this.add.tilemap('map', 32, 32);
		//lägger till bilden som krävs för att vissa map
		var tileset = map.addTilesetImage('tiles');
		
		//skapar layer att gå på
		layer = map.createLayer(0);
		//världen matchar lagrets storlek
		layer.resizeWorld();

		map.setCollisionBetween(0, 0);
		//om någon går på tilen med indexet triggas functionen
		map.setTileIndexCallback(6, this.resetPlayer, this);
		map.setTileIndexCallback(12, this.nextLevel, this);

		//skapar spelare
		player = this.add.sprite(100, 460, 'player');
		//ger position till client center karaktär
		player.anchor.setTo(0.5, 0.5);

		//animationer till spelare
		player.animations.add('idle', [0,1], 1, true);
		player.animations.add('jump', [2], 1, true);
		player.animations.add('run', [3,4,5,6,7,8], 7, true);

		//physics på spelaren
		this.physics.arcade.enable(player);

		//cameran följer spelare
		this.camera.follow(player);

		//colliderar med världen utanför om faller ut
		player.body.collideWorldBounds = true;

		//kontroller inför rörelse
		controls = {
			right: this.input.keyboard.addKey(Phaser.Keyboard.D),
			left: this.input.keyboard.addKey(Phaser.Keyboard.A),
			up: this.input.keyboard.addKey(Phaser.Keyboard.W)
		};

		//skapar fiender utifrån function ovan(kallar på enemyClown)
		enemy1 = new enemyClown (0, game, player.x + 200, player.y);
		enemy2 = new enemyClown (0, game, player.x + 970, player.y - 125);
	},

	update: function () {

		//gör så att i varje update stannar går velocity tillbaka till 0 efter rörelsen
		//(han fortsätter inte röra sig efter du slutat hålla inne w)
		player.body.velocity.x = 0;

		//skapar collusion mellan layer och spelaren
		this.physics.arcade.collide(player, layer);
		//controller och animationer + skapar hopp
		if(controls.up.isDown && (player.body.onFloor() || 
			player.body.touching.down) && this.time.now > JumpT){
			player.animations.play('jump');
			player.body.velocity.y = -500;
			JumpT = this.time.now + 750;
		}
		if(controls.right.isDown){
			player.animations.play('run');
			player.scale.setTo(1, 1);
			player.body.velocity.x += speed;
		}
		if(controls.left.isDown){
			player.animations.play('run');
			player.scale.setTo(-1, 1);
			player.body.velocity.x -= speed;
		}
		if(player.body.velocity.x == 0 && player.body.velocity.y == 0){
			player.animations.play('idle');
		}

		//om spelare colliderar med monster reset till början
		if(checkOverlap(player, enemy1.clown)){
			this.resetPlayer();

		}
		if(checkOverlap(player, enemy2.clown)){
			this.resetPlayer();

		}

	},
	//tillbaka till början
	resetPlayer: function() {
		// (start position)
		player.reset(100, 460);
	},
	//function byter level
	nextLevel: function() {

		this.state.start('Level2');
	},
	
}

function checkOverlap(spriteA, spriteB){
		//kollar var spritesen är, lägger i var
		var boundsA = spriteA.getBounds();
		var boundsB = spriteB.getBounds();
		//kollar om de är på varandra
		return Phaser.Rectangle.intersects(boundsA, boundsB);
	}