var Game = {};

Game.Boot = function(game){


};

Game.Boot.prototype = {
	init:function(){
		//max en grej på skärmen, mus detta fall
		this.input.maxPointers = 1;
		//spelet fryser ej när jag drar ur musen
		this.stage.disableVisibilityChange = true;
	},
	preload:function(){
		//Laddningsbar
		this.load.image('preloaderBar','assets/preloader.png');

	},

	create:function(){
		//startar nästa state
		this.state.start('Preloader');
	}

}