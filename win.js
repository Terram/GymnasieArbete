Game.win = function (game) {};

var map;
var layer;
var player;
var controls ={};
var speed = 200;
var JumpT = 0;
var winText;

Game.win.prototype ={
	create:function(game) {
		this.physics.arcade.gravity.y = 1000;

		this.stage.backgroundColor = '#3A5963';
		map = this.add.tilemap('map4', 32, 32);
		var tileset = map.addTilesetImage('tiles');
		
		layer = map.createLayer(0);
		layer.resizeWorld();

		map.setCollisionBetween(0, 3);

		map.setTileIndexCallback(6, this.resetPlayer, this);
		map.setTileIndexCallback(12, this.nextLevel, this);

		player = this.add.sprite(100, 460, 'player');
		player.anchor.setTo(0.5, 0.5);

		player.animations.add('idle', [0,1], 1, true);
		player.animations.add('jump', [2], 1, true);
		player.animations.add('run', [3,4,5,6,7,8], 7, true);

		this.physics.arcade.enable(player);

		this.camera.follow(player);

		player.body.collideWorldBounds = true;

		controls = {
			right: this.input.keyboard.addKey(Phaser.Keyboard.D),
			left: this.input.keyboard.addKey(Phaser.Keyboard.A),
			up: this.input.keyboard.addKey(Phaser.Keyboard.W),
			r: this.input.keyboard.addKey(Phaser.Keyboard.R)
		};
		winText = this.add.text(game.world.centerX - 200, game.world.centerY, 'Restart game with r', {font: '32px Arial', Fill:'#fff'});
		winText.fixedToCamera = true;
	},

	update: function () {
		player.body.velocity.x = 0;

		this.physics.arcade.collide(player, layer);
		if(controls.up.isDown && (player.body.onFloor() || 
			player.body.touching.down) && this.time.now > JumpT){
			player.animations.play('jump');
			player.body.velocity.y = -500;
			JumpT = this.time.now + 750;
		}
		if(controls.right.isDown){
			player.animations.play('run');
			player.scale.setTo(1, 1);
			player.body.velocity.x += speed;
		}
		if(controls.left.isDown){
			player.animations.play('run');
			player.scale.setTo(-1, 1);
			player.body.velocity.x -= speed;
		}
		if(player.body.velocity.x == 0 && player.body.velocity.y == 0){
			player.animations.play('idle');
		}
		if(controls.r.isDown){
			this.state.start('Level1');
		}

	}
}